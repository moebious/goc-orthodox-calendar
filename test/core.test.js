/*
var assert = require('assert')
var core = require('../src/core')();
*/

//import assert from 'assert'
import core from '../src/core'
import {assert} from 'chai'
let coreLib = core()
//coreLib.caching = false


describe('core', function () {
 describe('#dayOfWeek', function(){
   it('should return something', function(){
     var sunday = 0 
     var actual = coreLib.getDayOfWeek(0)
     assert.equal(actual, sunday) //pascha should be a sunday
   })
 })
 describe('#convertToOldCalendar', function(){
   it('should return the correct value', function(){
    var actual = coreLib.convertToOldCalendar(8, 28, 2019)
    
    console.log(actual)
    assert.equal(actual.year === 2019, true, "should be the year 2019")
    assert.equal(actual.month === 8, true, "should be the 8th month")
    assert.equal(actual.day === 15, true, "should be the 15th day")

   })
 })
 describe('#gregorianToJd should be correct', function(){
   it('should return a different value for a different day- simple.. right?', function(){
    const day1 = new Date(2019, 7, 28)
    const day2 = new Date(2019, 7, 29)

    var gregJd1 = coreLib.gregorianToJd(day1.getFullYear(), day1.getMonth() + 1, day1.getDate())
    var gregJd2 = coreLib.gregorianToJd(day2.getFullYear(), day2.getMonth() + 1, day2.getDate())
    
    //assert.equal(gregJd1 !== gregJd2, true, "these should NOT be the same")
    assert.notEqual(gregJd1, gregJd2, "these should not match")
    
   })
   it('should be correct when calling calulateDay', function(){
    var jd1 = coreLib.calculateDay(8, 29, 2019).jd
    var jd2 = coreLib.calculateDay(8, 28, 2019).jd
    assert.notEqual(jd1, jd2, "these should not be the same")
    assert.equal(jd2, 2458724)
    assert.equal(jd1, 2458725)
    console.log(jd1, jd2)
   })
 })
 describe('convert gregorian to julian', function(){
   it('should be about 13 days away', function(){
     const day = new Date(2019, 7, 28)

     var gregJd = coreLib.gregorianToJd(2019, 8, 28)
     var actual = coreLib.jdToJulian(gregJd)
     console.log(actual)
     assert.equal(actual[0] === 2019, true, "should be the year 2019")
     assert.equal(actual[1] === 8, true, "should be the 8th month")
     assert.equal(actual[2] === 15, true, "should be the 15th day")
   })
 })
 describe('#calculatePascha', function(){
   it('should return an number', function(){
     var actual = coreLib.calculatePascha(2016)
     assert.equal(actual > 0, true)
   })
 })
 describe('#calculateYear', function(){
   it('should return a obj value', function(){
     var actual = coreLib.calculateYear(8, 18, 2019)
     assert.equal(actual.peterpaul > 0, true)
   })
 })
 describe('#calculatePentecost', function(){
   it('should return a string that includes the word Pentecost', function(){
     var actual = coreLib.calculatePentecost(15)
     assert.equal(actual.indexOf('Pentecost') > -1, true)
   })
 })
 describe('#calculateDay', function(){
  it('should return an object that contains all the day stuff', function(){
    var actual = coreLib.calculateDay(8, 29, 2019)
    console.log(actual)
    assert.equal(typeof actual === 'object', true)
  })
})

});

