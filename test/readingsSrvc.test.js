import assert from 'assert'
import { expect } from 'chai'
import srv from '../src/readingsService'
class InvalidArgumentError extends Error {}

describe('readingService', function(){
  describe('#retrieveReadings', function(){
    it('should return a value', function(){
      var day = {}
      day.noMemorial = true
      day.matinsGospel = true
      day.noMatinsGospel = true
      day.noParamias = false
      day.getParamias = true
      day.eDay = 234
      day.gDay = 499
      day.pDay = -6
      day.fDay = 99
      day.month = 3
      day.day = 25
      day.dayOfWeek = 1
      day.year = 2019
      day.menaionMonth = 3
      day.menaionDay = 25
      day.feastLevel = 2 //up to 8 also can be null
      var actual = srv.retrieveReadings(day)
      console.log(`length of return array= ${actual.length}`)
      console.log(JSON.stringify(actual, ' '))
      assert(typeof actual === 'object', 'this should be something')
      
    })
    it('should throw an InvalidArgumentError if passed undefined', function() {
      expect(function () { srv.retrieveReadings(undefined) }).to.throw(/defined/)
    })
    it('should throw an InvalidArgumentError if passed object with no day property', function() {
      var day = {}
      day.noMemorial = true
      day.matinsGospel = true
      day.noMatinsGospel = true
      day.noParamias = false
      day.getParamias = true
      day.eDay = 234
      day.gDay = 499
      day.pDay = -6
      day.fDay = 99
      day.month = 3
      //day.day = 25
      day.dayOfWeek = 1
      day.year = 2019
      day.menaionMonth = 3
      day.menaionDay = 25
      expect(function () { srv.retrieveReadings(day) }).to.throw(Error)
    })
  })
})