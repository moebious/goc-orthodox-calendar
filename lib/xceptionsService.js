'use strict';

var exceptions = require('./data/xceptions');

module.exports = function () {
  var getByDate = function getByDate(month, day, year) {

    var item = exceptions.find(function (value) {
      return value.xcYear === year && value.xcMonth === month && value.xcDay === day;
    });

    return item;
  };

  return {
    getByDate: getByDate
  };
};

//module.exports = exceptionSrvc()
//var exceptions = require('./data/xceptions.js')