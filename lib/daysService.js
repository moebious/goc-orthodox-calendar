'use strict';

var days = require('./data/days');

var log = function log(msg) {
  console.log(msg);
};
//var days = require('./data/days.js')

module.exports = function () {
  var getDaysForDayCalculation = function getDaysForDayCalculation(pDay, alternativepDay, month, day) {
    //log(`using pDay=${pDay}, alternativepDay=${alternativepDay}, month=${month}, day=${day}`)
    var items = [];
    if (alternativepDay) {
      items = days.filter(function (value) {
        return value.daPday === pDay || value.daPday === alternativepDay || value.daMonth === month && value.daDay === day;
      });
    } else {
      items = days.filter(function (value) {
        return value.daPday === pDay || value.daMonth === month && value.daDay === day;
      });
    }

    return items || [];
  };

  var getByMonthDay = function getByMonthDay(month, day) {

    var item = days.find(function (value) {
      return value.daDay === day && value.daMonth === month;
    });

    return item;
  };

  return {
    getByMonthDay: getByMonthDay,
    getDaysForDayCalculation: getDaysForDayCalculation
  };
};