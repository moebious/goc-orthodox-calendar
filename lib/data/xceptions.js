"use strict";

var exceptionList = [{
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 3,
  "xcDay": 8,
  "xcNewMonth": 3,
  "xcNewDay": 9,
  "xcNote": "(Note: The service for the 40 Martyrs is held today.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 3,
  "xcDay": 9,
  "xcNewMonth": 99,
  "xcNewDay": 9,
  "xcNote": "(Note: The service for the 40 Martyrs is transferred to March 8.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 4,
  "xcDay": 25,
  "xcNewMonth": 99,
  "xcNewDay": 99,
  "xcNote": "(Note: The service for St Mark is transferred to May 4.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 5,
  "xcDay": 4,
  "xcNewMonth": 4,
  "xcNewDay": 25,
  "xcNote": "(Note: The service for St Mark is held today.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 4,
  "xcDay": 30,
  "xcNewMonth": 99,
  "xcNewDay": 99,
  "xcNote": "(Note: The service for St James is transferred to May 5.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 5,
  "xcDay": 5,
  "xcNewMonth": 4,
  "xcNewDay": 30,
  "xcNote": "(Note: The service for St James is held today.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 6,
  "xcDay": 19,
  "xcNewMonth": 99,
  "xcNewDay": 99,
  "xcNote": "(Note: The service for St Jude is transferred to June 20.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2016,
  "xcMonth": 6,
  "xcDay": 20,
  "xcNewMonth": 6,
  "xcNewDay": 19,
  "xcNote": "(Note: The service for St Jude is held today.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2018,
  "xcMonth": 4,
  "xcDay": 7,
  "xcNewMonth": 99,
  "xcNewDay": 99,
  "xcNote": "(Note: The service for St Tikhon is transferred to April 10.)",
  "xcFlag": 0
}, {
  "xcId": 0,
  "xcYear": 2018,
  "xcMonth": 4,
  "xcDay": 10,
  "xcNewMonth": 4,
  "xcNewDay": 7,
  "xcNote": "(Note: The service for St Tikhon is held today.)",
  "xcFlag": 0
}];

module.exports = exceptionList;