'use strict';

var readings = require('./data/readings');

var zachalos = require('./data/zachalos');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InvalidArgumentError = function (_Error) {
  _inherits(InvalidArgumentError, _Error);

  function InvalidArgumentError() {
    _classCallCheck(this, InvalidArgumentError);

    return _possibleConstructorReturn(this, (InvalidArgumentError.__proto__ || Object.getPrototypeOf(InvalidArgumentError)).apply(this, arguments));
  }

  return InvalidArgumentError;
}(Error);

var indexedReadings = readings.reduce(function (acc, cur) {
  acc[cur.reBook + '-' + cur.reNum] = cur;
  return acc;
}, {});

var indexedZachalos = zachalos.reduce(function (acc, cur) {
  acc[cur.zaBook + '-' + cur.zaNum] = cur;
  return acc;
}, {});

var joined = [];

Object.keys(indexedReadings).forEach(function (key) {
  var merged = Object.assign({}, indexedReadings[key], indexedZachalos[key]);
  joined.push(merged);
});

/**
 * Adds the amount of days to the given date
 * @param {Date} date 
 * @param {Number} amount 
 * @return {Date} a new resulting Date object
 */
var addDays = function addDays(date, amount) {
  var t = date.getDate() + amount;
  return new Date(date.setDate(t));
};

/**
 * Gets readings for the given day object
 * 
 * the day object literal looks like this
 * day = {
 *    noMemorial: bool
 *    gDay:       number
 *    eDay:       number
 *    fDay:       number
 *    matinsGospel bool
 *    noMatinsGospel bool
 *    noParamias  bool
 *    month       number
 *    day         number
 *    dayOfWeek   number
 *    year        number
 *    menaionMonth number
 *    menaionDay  number
 *    getParamias bool
 *    }
 * @param {Object} day an object literal with many values in it see below
 */

var retrieveReadings = function retrieveReadings(day) {
  try {
    retrieveReadingsGuard(day);
  } catch (e) {
    throw e;
  }
  console.log('using this object to fetch readings ' + JSON.stringify(day, null, ' '));
  var conditions = [];
  var nomem = '';
  if (day.noMemorial) {
    nomem = ' && val.reDesc !== \'Departed\'';
  }
  if (day.gDay !== 499) {
    conditions.push('(val.rePday === ' + day.gDay + ' && val.reType === \'Gospel\' ' + nomem + ')');
  }
  if (day.eDay !== 499) {
    conditions.push('(val.rePday === ' + day.eDay + ' && val.reType === \'Epistle\' ' + nomem + ')');
  }
  conditions.push('(val.rePday === ' + day.pDay + ' && val.reType !== \'Epistle\' && val.reType !== \'Gospel\')');
  if (day.fDay && day.fDay !== 499) {
    conditions.push('(val.rePday === ' + day.fDay + ')');
  }
  if (day.matinsGospel) {
    var mg = day.matinsGospel + 700;
    conditions.push('(val.rePday === ' + mg + ')');
  }
  var notMatinsGospel = '';
  var notVespers = '';
  if (day.noMatinsGospel) {
    notMatinsGospel = '&& val.reType !== \'Matins Gospel\'';
  } else {
    notMatinsGospel = '';
  }
  if (day.noParamias) {
    notVespers = '&& val.reType !== \'Vespers\'';
  } else {
    notVespers = '';
  }
  // no readings for leavtaking annunciation (day after annunciation)
  // on non-liturgy day
  var descNotTheotokos = void 0;
  if (day.month === 3 && day.day === 26 && (day.dayOfWeek === 1 || day.dayOfWeek === 2 || day.dayOfWeek === 4)) {
    descNotTheotokos = '&& val.reDesc !== \'Theotokos\'';
  } else {
    descNotTheotokos = '';
  }
  conditions.push('((val.reMonth === ' + day.menaionMonth + ' && val.reDay === ' + day.menaionDay + ') ' + notMatinsGospel + ' ' + notVespers + ' ' + descNotTheotokos + ')');

  if (day.getParemias) {
    var currentDate = new Date(day.year, day.month - 1, day.day);
    var nextDay = addDays(currentDate, 1);
    conditions.push('(val.reMonth === ' + (nextDay.getMonth() + 1) + ' && val.reDay === ' + nextDay.getDate() + ' && val.reType === \'Vespers\')');
  }

  // we may need to move the filtering part out at some point

  //(val.rePday === 123 && val.reType === 'Gospel' ) || (val.rePday === 123 && val.reType === 'Epistle' ) || (val.rePday === 123 && val.reType !== 'Epistle' && val.reType !== 'Gospel') || ((val.reMonth === 0 && val.reDay === 0)   )

  var query = conditions.join(' || ');
  //console.log(query)
  var ret = joined.filter(function (val) {
    return eval(query);
  });

  //console.log(`joined length=${joined.length}`)
  //console.log(`filtered length = ${ret.length}`)
  var types = [];
  var descs = [];
  var books = [];
  var displays = [];
  var sdisplays = [];
  var nums = [];
  var idx = [];

  var spliceAllAt = function spliceAllAt(index) {
    types.splice(index, 1);
    descs.splice(index, 1);
    books.splice(index, 1);
    displays.splice(index, 1);
    sdisplays.splice(index, 1);
    nums.splice(index, 1);
  };

  // load arrays
  ret.forEach(function (item) {
    types.push(item.reType);
    descs.push(item.reDesc);
    books.push(item.reBook);
    displays.push(item.zaDisplay);
    sdisplays.push(item.zaSdisplay);
    nums.push(item.reNum);
    idx.push(item.reIndex);
  });

  if (day.fDay === 1027) {
    var i = idx.findIndex(function (item) {
      return item === 811;
    });
    var j = idx.findIndex(function (item) {
      return item === 911;
    });
    if (i > -1) {
      spliceAllAt(i);
    }

    if (j > -1) {
      spliceAllAt(j);
    }
  }
  if (isLent(day.pDay, day.feastLevel)) {
    var _i = types.findIndex(function (item) {
      return item === "Matins Gospel";
    });
    if (_i > -1) {
      spliceAllAt(_i);
    }
  }
  var retVal = {
    types: types,
    descs: descs,
    displays: displays,
    sdisplays: sdisplays,
    nums: nums,
    idx: idx
  };
  console.log('Readings result = ' + JSON.stringify(retVal, null, ' '));

  return retVal;
};

var isLent = function isLent(pDay, feastLevel) {
  return pDay > -42 && pDay < -7 && feastLevel < 7;
};

var retrieveReadingsGuard = function retrieveReadingsGuard(argument) {
  if (argument === undefined) throw new InvalidArgumentError('argument must be defined');
  if (argument.day === undefined) throw new InvalidArgumentError('argument.day must be defined');
  if (argument.month === undefined) throw new InvalidArgumentError('argument.month must be defined');
  if (argument.year === undefined) throw new InvalidArgumentError('argument.year must be defined');
  if (argument.dayOfWeek === undefined) throw new InvalidArgumentError('argument.dayOfWeek must be defined');
  if (argument.eDay === undefined) throw new InvalidArgumentError('argument.eDay must be defined');
  if (argument.pDay === undefined) throw new InvalidArgumentError('argument.pDay must be defined');
  if (argument.gDay === undefined) throw new InvalidArgumentError('argument.gDay must be defined');
  if (argument.fDay === undefined) throw new InvalidArgumentError('argument.fDay must be defined');
  if (argument.noMemorial === undefined) throw new InvalidArgumentError('argument.noMemorial must be defined');
  if (argument.matinsGospel === undefined) throw new InvalidArgumentError('argument.matinsGospel must be defined');
  if (argument.noMatinsGospel === undefined) throw new InvalidArgumentError('argument.noMatinsGospel must be defined');
  if (argument.noParamias === undefined) throw new InvalidArgumentError('argument.noParamias must be defined');
  if (argument.getParamias === undefined) throw new InvalidArgumentError('argument.getParamias must be defined');
  if (argument.menaionMonth === undefined) throw new InvalidArgumentError('argument.menaionMonth must be defined');
  if (argument.menaionDay === undefined) throw new InvalidArgumentError('argument.menaionDay must be defined');
  if (argument.feastLevel === undefined) throw new InvalidArgumentError('argument.feastLevel must be defined');
};

module.exports = {
  retrieveReadings: retrieveReadings
};