
//var days = require('./data/days.js')

import days from './data/days'
const log = (msg) => {
  console.log(msg)
}
export default () =>{
  const getDaysForDayCalculation = (pDay, alternativepDay, month, day) => {
    //log(`using pDay=${pDay}, alternativepDay=${alternativepDay}, month=${month}, day=${day}`)
    let items = []
    if(alternativepDay){
      items = days.filter ( (value) => {
        return (value.daPday === pDay || value.daPday === alternativepDay) ||
        (value.daMonth === month && value.daDay === day)
      })
    } else {
      items = days.filter ( (value) => {
        return (value.daPday === pDay) ||
        (value.daMonth === month && value.daDay === day)
      })
    }

    return  items || []

  }

  const getByMonthDay = (month, day) => {
    
    var item = days.find((value) => {
      return (value.daDay === day && value.daMonth === month)
    })
    
    return item
  }

  return {
    getByMonthDay: getByMonthDay,
    getDaysForDayCalculation: getDaysForDayCalculation
  }

}

