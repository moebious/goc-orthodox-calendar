
import readings from './data/readings'
import zachalos from './data/zachalos'

class InvalidArgumentError extends Error {}

let indexedReadings = readings.reduce (function (acc, cur){
  acc[cur.reBook + '-' + cur.reNum] = cur
  return acc
}, {})

let indexedZachalos = zachalos.reduce (function (acc, cur) {
  acc[cur.zaBook + '-' + cur.zaNum] = cur
  return acc
}, {})

let joined = []

Object.keys(indexedReadings).forEach(function(key){
  let merged = Object.assign({}, indexedReadings[key], indexedZachalos[key])
  joined.push(merged)
})


/**
 * Adds the amount of days to the given date
 * @param {Date} date 
 * @param {Number} amount 
 * @return {Date} a new resulting Date object
 */
const addDays = (date, amount) => {
  let t = date.getDate() + amount
  return new Date(date.setDate( t ))
}

/**
 * Gets readings for the given day object
 * 
 * the day object literal looks like this
 * day = {
 *    noMemorial: bool
 *    gDay:       number
 *    eDay:       number
 *    fDay:       number
 *    matinsGospel bool
 *    noMatinsGospel bool
 *    noParamias  bool
 *    month       number
 *    day         number
 *    dayOfWeek   number
 *    year        number
 *    menaionMonth number
 *    menaionDay  number
 *    getParamias bool
 *    }
 * @param {Object} day an object literal with many values in it see below
 */

const retrieveReadings = (day) => {
  try{
    retrieveReadingsGuard (day)
  }catch (e) {
    throw e
  }
  console.log(`using this object to fetch readings ${JSON.stringify(day, null, ' ')}`)
  let conditions = []
  let nomem = ''
  if (day.noMemorial) {
    nomem = ' && val.reDesc !== \'Departed\''
  }
  if (day.gDay !== 499){
    conditions.push(`(val.rePday === ${day.gDay} && val.reType === 'Gospel' ${nomem})`)
  }
  if (day.eDay !== 499) {
    conditions.push(`(val.rePday === ${day.eDay} && val.reType === 'Epistle' ${nomem})`)
  }
  conditions.push(`(val.rePday === ${day.pDay} && val.reType !== 'Epistle' && val.reType !== 'Gospel')`)
  if (day.fDay && day.fDay !== 499) {
    conditions.push(`(val.rePday === ${day.fDay})`)
  }
  if (day.matinsGospel) {
    let mg = day.matinsGospel + 700
    conditions.push(`(val.rePday === ${mg})`)
  }
  let notMatinsGospel = ''
  let notVespers = ''
  if (day.noMatinsGospel) {
    notMatinsGospel = `&& val.reType !== 'Matins Gospel'`
  } else {
    notMatinsGospel = ``
  }
  if (day.noParamias) {
    notVespers = `&& val.reType !== 'Vespers'`
  } else {
    notVespers = ``
  }
  // no readings for leavtaking annunciation (day after annunciation)
  // on non-liturgy day
  let descNotTheotokos
  if (day.month === 3 && day.day === 26 && (day.dayOfWeek === 1 || day.dayOfWeek === 2 || day.dayOfWeek === 4)) {
    descNotTheotokos = `&& val.reDesc !== 'Theotokos'`
  } else {
    descNotTheotokos = ``
  }
  conditions.push(`((val.reMonth === ${day.menaionMonth} && val.reDay === ${day.menaionDay}) ${notMatinsGospel} ${notVespers} ${descNotTheotokos})`)

  if (day.getParemias) {
    let currentDate = new Date(day.year, day.month - 1, day.day)
    let nextDay = addDays (currentDate, 1)
    conditions.push(`(val.reMonth === ${nextDay.getMonth() + 1} && val.reDay === ${nextDay.getDate()} && val.reType === 'Vespers')`)
  }

  // we may need to move the filtering part out at some point

  //(val.rePday === 123 && val.reType === 'Gospel' ) || (val.rePday === 123 && val.reType === 'Epistle' ) || (val.rePday === 123 && val.reType !== 'Epistle' && val.reType !== 'Gospel') || ((val.reMonth === 0 && val.reDay === 0)   )

  let query = conditions.join(' || ')
  //console.log(query)
  let ret  = joined.filter(function(val) {
    return eval(query)
  })

  //console.log(`joined length=${joined.length}`)
  //console.log(`filtered length = ${ret.length}`)
  let types = []
  let descs = []
  let books = []
  let displays = []
  let sdisplays = []
  let nums = []
  let idx = []

  const spliceAllAt = (index) => {
    types.splice(index, 1)
    descs.splice(index, 1)
    books.splice(index, 1)
    displays.splice(index, 1)
    sdisplays.splice(index, 1)
    nums.splice(index, 1)
  }

  // load arrays
  ret.forEach(function (item) {
    types.push (item.reType)
    descs.push (item.reDesc)
    books.push (item.reBook)
    displays.push (item.zaDisplay)
    sdisplays.push (item.zaSdisplay)
    nums.push (item.reNum)
    idx.push (item.reIndex)
  })


  if (day.fDay === 1027) {
    let i = idx.findIndex(function(item){
      return item === 811
    })
    let j = idx.findIndex (function (item) {
      return item === 911
    })
    if (i > -1) {
      spliceAllAt(i)
    }

    if (j > -1) {
      spliceAllAt(j)
    }

  }
  if (isLent(day.pDay, day.feastLevel)) {
    let i = types.findIndex (function (item) {
      return item === "Matins Gospel"
    })
    if (i > -1) {
      spliceAllAt(i)
    }
  }
  let retVal =  {
    types,
    descs,
    displays,
    sdisplays,
    nums,
    idx
  }
  //console.log(`Readings result = ${JSON.stringify(retVal, null, ' ')}`)

  return retVal
  
}

const isLent = (pDay, feastLevel) => {
  return (pDay > -42 && pDay < -7 && feastLevel < 7)
}

const retrieveReadingsGuard = (argument) => {
  if (argument === undefined) throw new InvalidArgumentError('argument must be defined')
  if (argument.day === undefined) throw new InvalidArgumentError('argument.day must be defined')
  if (argument.month === undefined) throw new InvalidArgumentError('argument.month must be defined')
  if (argument.year === undefined) throw new InvalidArgumentError('argument.year must be defined')
  if (argument.dayOfWeek === undefined) throw new InvalidArgumentError('argument.dayOfWeek must be defined')
  if (argument.eDay === undefined) throw new InvalidArgumentError('argument.eDay must be defined')
  if (argument.pDay === undefined) throw new InvalidArgumentError('argument.pDay must be defined')
  if (argument.gDay === undefined) throw new InvalidArgumentError('argument.gDay must be defined')
  if (argument.fDay === undefined) throw new InvalidArgumentError('argument.fDay must be defined')
  if (argument.noMemorial === undefined) throw new InvalidArgumentError('argument.noMemorial must be defined')
  if (argument.matinsGospel === undefined) throw new InvalidArgumentError('argument.matinsGospel must be defined')
  if (argument.noMatinsGospel === undefined) throw new InvalidArgumentError('argument.noMatinsGospel must be defined')
  if (argument.noParamias === undefined) throw new InvalidArgumentError('argument.noParamias must be defined')
  if (argument.getParamias === undefined) throw new InvalidArgumentError('argument.getParamias must be defined')
  if (argument.menaionMonth === undefined) throw new InvalidArgumentError('argument.menaionMonth must be defined')
  if (argument.menaionDay === undefined) throw new InvalidArgumentError('argument.menaionDay must be defined')
  if (argument.feastLevel === undefined) throw new InvalidArgumentError('argument.feastLevel must be defined')
}

export default {
  retrieveReadings: retrieveReadings
}