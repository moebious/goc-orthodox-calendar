/*
const exceptionSrvc = require('./xceptionsService')
const daysSrvc = require('./daysService')
const easter = require('date-easter')
*/


import daysSrvc from './daysService'
import exceptionSrvc from './xceptionsService'
import assert from 'assert'

const log = (msg) => {
  console.log(msg)
}

// "plugins": ["transform-es2015-modules-simple-commonjs"]

export default () =>{
  let caching = true;
  const DO_JUMP = 0
  const FastLevels = [
    '',
    'Wine & Oil',
    'Fish, Wine & Oil',
    'Wine & Oil',
    'Fish, Wine & Oil',
    'Wine',
    'Wine, Oil & Caviar',
    'Meat Fast',
    'Strict Fast (Wine & Oil)',
    'Strict Fast',
    '',
    'Fast Free'
  ]

  const GREGORIAN = {
    EPOCH: 1721425.5,
    MONTHS: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  }

  const JULIAN = {
    EPOCH: 1721423.5,
    MONTHS: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  }

  const FAST_LEVELS = []
  const FASTS = []

  const jdToJulian = function (jd) {
    var b0, c0, d0, e0, year, month, day;

    b0 = Math.floor (jd + 0.5) + 1524;
    c0 = Math.floor ((b0 - 122.1) / 365.25);
    d0 = Math.floor (365.25 * c0);
    e0 = Math.floor ((b0 - d0) / 30.6001);

    month = Math.floor (e0 < 14 ? e0 - 1 : e0 - 13);
    year = Math.floor (month > 2 ? c0 - 4716 : c0 - 4715);
    day = b0 - d0 - Math.floor (30.6001 * e0);

    // If year is less than 1, subtract one to convert from
    // a zero based date system to the common era system in
    // which the year -1 (1 B.C.E) is followed by year 1 (1 C.E.).
    if (year < 1) {
      year -= 1;
    }

    return [ year, month, day ];
  };


  /**
   *  calculatePascha returns the Julian Day Number
   *  of pascha for a given year
   * @param {*} year 
   * @returns number
   */
  const calculatePascha = (year) => {
    let jg = 0
    let x = 0
    if (year > 1582) {
      jg = 10
      x = Math.floor (year/100) - 16
      if (x > 0) {
        jg += (Math.floor (x / 4) *3) + x % 4
      }
    }
    let a = year % 4
    let b = year % 7
    let c = year % 19
    let z = ( (19 * c) + 15) % 30
    let e = ( (2 * a) + (4 * b) - z + 34) % 7
    let month = Math.floor( (z + e + 114) / 31)
    let day = ( (z + e + 114) % 31) + 1
    return jg + gregorianToJd(year, month, day)
  }

  const getDayOfWeek = (pDay) => {
    return (7 + (pDay % 7)) % 7
  }

  

  /**
   * The heart of this system is the concept of a "paschal year,"
   * which begins with Zacchaeus Sunday and continues to the next one.
   * Within the paschal year, every day has an integer value based
   * on its relation to Pascha, which we call the "pday." 
   * The pday of Pascha is 0, the pday of Palm Sunday is -7, 
   * the pday of Ascension is 39, the pday of Pentecost is 49, etc.
   * This function will return pdays (and other info) of 
   * significant days of the paschal year.
   * 
   */
  const calculateYear = (year) => {
   
    const calculate = (year) => {
    
    const retVal = {
      paschaJd: 0,
      previousPaschaJd: 0,
      previousPascha: 0,
      nextPaschaJd: 0,
      nextPascha: 0,
      finding: 0,
      annunciation: 0,
      annunciationWeekday: 0,
      peterpaul: 0,
      beheading: 0,
      nativityTheotokos: 0,
      elevation: 0,
      elevationWeekday: 0,
      saturdayBeforeElevation: 0,
      sundayBeforeElevation: 0,
      saturdayAfterElevation: 0,
      sundayAfterElevation: 0,
      lucanJump: 0,
      fathersSix: 0,
      fathersSeven: 0,
      demetriusSaturday: 0,
      synaxisUnmercenaries: 0,
      nativity: 0,
      nativityWeekday: 0,
      foreFathers: 0,
      saturdayBeforeNativity: 0,
      sundayBeforeNativity: 0,
      saturdayAfterNativity: 0,
      sundayAfterNativity: 0,
      theophany: 0,
      theophanyWeekday: 0,
      saturdayBeforeTheophany: 0,
      sundayBeforeTheophany: 0,
      saturdayAfterTheophany: 0,
      sundayAfterTheophany:0,
      reserves: [],
      noDaily: [],
      floats: [],
      noParamias: [],
      getParamias: [],
      noDailyAnn: false
    }
    retVal.paschaJd = calculatePascha(year)
    retVal.finding = gregorianToJd(year, 2, 24) - retVal.paschaJd
    retVal.annunciation = gregorianToJd(year, 3, 25) - retVal.paschaJd
    retVal.annunciationWeekday = getDayOfWeek(retVal.annunciation)
    retVal.peterpaul = gregorianToJd(year, 6, 29) - retVal.paschaJd
    retVal.beheading = gregorianToJd(year, 8, 29) - retVal.paschaJd
    retVal.nativityTheotokos = gregorianToJd(year, 9, 8) - retVal.paschaJd
    retVal.elevation = gregorianToJd(year, 9, 14) - retVal.paschaJd
    retVal.elevationWeekday = getDayOfWeek(retVal.elevation)
    retVal.saturdayBeforeElevation = prevSaturday(retVal.elevation, retVal.elevationWeekday)
    retVal.sundayBeforeElevation = prevSunday(retVal.elevation, retVal.elevationWeekday)
    retVal.saturdayAfterElevation = upcomingSaturday(retVal.elevation, retVal.elevationWeekday)
    retVal.sundayAfterElevation = upcomingSunday(retVal.elevation, retVal.elevationWeekday)
    //TODO what is 168 here
    retVal.lucanJump = 168 - retVal.sundayAfterElevation
    retVal.fathersSix = getFathersSix(retVal.paschaJd, year)
    retVal.fathersSeven = getFathersSeven(retVal.paschaJd, year)
    retVal.demetriusSaturday = getDemetriusSaturday(retVal.paschaJd, year)
    retVal.synaxisUnmercenaries = getSynaxisOfUnmercenaries(retVal.paschaJd, year)
    retVal.nativity = gregorianToJd(year, 12, 25) - retVal.paschaJd
    retVal.nativityWeekday = getDayOfWeek(retVal.nativity)
    retVal.foreFathers = retVal.nativity - 14 + ((7 - retVal.nativityWeekday) % 7)
    retVal.saturdayBeforeNativity = prevSaturday(retVal.nativity, retVal.nativityWeekday)
    retVal.sundayBeforeNativity = prevSunday(retVal.nativity, retVal.nativityWeekday)
    retVal.saturdayAfterNativity = upcomingSaturday(retVal.nativity, retVal.nativityWeekday)
    retVal.sundayAfterNativity = upcomingSunday(retVal.nativity, retVal.nativityWeekday)
    //Theophany
    retVal.theophany = gregorianToJd(year + 1, 1, 6) - retVal.paschaJd
    retVal.theophanyWeekday = getDayOfWeek(retVal.theophany)
    retVal.saturdayBeforeTheophany = prevSaturday(retVal.theophany, retVal.theophanyWeekday)
    retVal.sundayBeforeTheophany = prevSunday(retVal.theophany, retVal.theophanyWeekday)
    retVal.saturdayAfterTheophany = upcomingSaturday(retVal.theophany, retVal.theophanyWeekday)
    retVal.sundayAfterTheophany = upcomingSunday(retVal.theophany, retVal.theophanyWeekday)

    let floats = []
    for (var i = 1001; i < 1038; i++) {
      floats[i] = 499
    }
    floats[1001] = retVal.fathersSix
    floats[1002] = retVal.fathersSeven
    floats[1003] = retVal.demetriusSaturday
    floats[1004] = retVal.synaxisUnmercenaries
    if (retVal.saturdayBeforeElevation === retVal.nativityTheotokos) {
      floats[1005] = retVal.elevation - 1
    } else {
      floats[1006] = retVal.saturdayBeforeElevation
    }
    floats[1007] = retVal.sundayBeforeElevation
    floats[1008] = retVal.saturdayAfterElevation
    floats[1009] = retVal.sundayAfterElevation
    floats[1010] = retVal.foreFathers
    if (retVal.saturdayBeforeNativity === retVal.nativity - 1){
      floats[1013] = retVal.nativity - 2
      floats[1012] = retVal.sundayBeforeNativity
      floats[1015] = retVal.nativity - 1
    } else if (retVal.sundayBeforeNativity === retVal.nativity - 1) {
      floats[1013] = retVal.nativity - 3
      floats[1011] = retVal.saturdayBeforeNativity
      floats[1016] = retVal.nativity - 1
    } else {
      floats[1014] = retVal.nativity - 1
      floats[1011] = retVal.saturdayBeforeNativity
      floats[1012] = retVal.sundayBeforeNativity
    }

    if (retVal.nativityWeekday === 0) {
      floats[1017] = retVal.saturdayAfterNativity
      floats[1020] = retVal.nativity + 1
      floats[1024] = retVal.sundayBeforeTheophany
      floats[1026] = retVal.theophany - 1
    } else if (retVal.nativityWeekday === 1) {
      floats[1017] = retVal.saturdayAfterNativity
      floats[1021] = retVal.sundayAfterNativity
      floats[1023] = retVal.theophany - 5
      floats[1026] = retVal.theophany - 1
    } else if (retVal.nativityWeekday === 2) {
      floats[1019] = retVal.saturdayAfterNativity
      floats[1021] = retVal.sundayAfterNativity
      floats[1027] = retVal.saturdayBeforeTheophany
      floats[1023] = retVal.theophany - 5
      floats[1025] = retVal.theophany - 2
    } else if (retVal.nativityWeekday === 3) {
      floats[1019] = retVal.saturdayAfterNativity
      floats[1021] = retVal.sundayAfterNativity
      floats[1022] = retVal.saturdayBeforeTheophany
      floats[1028] = retVal.sundayBeforeTheophany
      floats[1025] = retVal.theophany - 3
    } else if (retVal.nativityWeekday === 4 || retVal.nativityWeekday === 5) {
      floats[1019] = retVal.saturdayAfterNativity
      floats[1021] = retVal.sundayAfterNativity
      floats[1022] = retVal.saturdayBeforeTheophany
      floats[1024] = retVal.sundayBeforeTheophany
      floats[1026] = retVal.theophany - 1
    } else if (retVal.nativityWeekday === 6) {
      floats[1018] = retVal.nativity + 6
      floats[1021] = retVal.sundayAfterNativity
      floats[1022] = retVal.saturdayBeforeTheophany
      floats[1024] = retVal.sundayBeforeTheophany
      floats[1026] = retVal.theophany - 1
    }
    floats[1029] = retVal.saturdayAfterTheophany
    floats[1030] = retVal.sundayAfterTheophany
    
    if (retVal.annunciationWeekday === 6) {
      floats[1032] = retVal.annunciation - 1
      floats[1033] = retVal.annunciation
      retVal.noDailyAnn = true
    } else if (retVal.annunciationWeekday === 0) {
      floats[1034] = retVal.annunciation
    } else if (retVal.annunciationWeekday === 1) {
      floats[1035] = retVal.annunciation
    } else {
      floats[1036] = retVal.annunciation - 1
      floats[1037] = retVal.annunciation
    }

    

    retVal.noDaily.push(retVal.sundayBeforeTheophany)
    retVal.noDaily.push(retVal.sundayAfterTheophany)
    retVal.noDaily.push(retVal.theophany - 5)

    if (retVal.saturdayBeforeTheophany !== (retVal.theophany - 1)) {
      retVal.noDaily.push(retVal.theophany - 1)
    }
    retVal.noDaily.push(retVal.theophany)
    
    if (retVal.saturdayAfterTheophany === retVal.theophany + 1) {
      retVal.noDaily.push(retVal.theophany = 1)
    }
    retVal.noDaily.push(retVal.foreFathers)
    retVal.noDaily.push(retVal.sundayBeforeNativity)
    retVal.noDaily.push(retVal.nativity - 1)
    retVal.noDaily.push(retVal.nativity)
    retVal.noDaily.push(retVal.nativity + 1)
    retVal.noDaily.push(retVal.sundayAfterNativity)
    if (retVal.noDailyAnn) {
      retVal.noDaily.push(retVal.annunciation)
    }

    retVal.previousPaschaJd = calculatePascha(year - 1)
    retVal.previousPascha = retVal.previousPaschaJd - retVal.paschaJd
    retVal.nextPaschaJd = calculatePascha (year + 1)
    retVal.nextPascha = retVal.nextPaschaJd - retVal.paschaJd
    let val = retVal.nextPascha - 84
    let extraSundays = Math.floor ((val - retVal.sundayAfterTheophany) / 7)
    retVal.extraSundays = extraSundays
    if(extraSundays){
      let begin = retVal.foreFathers + retVal.lucanJump + 7
      for (var x = begin; x <= 266; x += 7){ 
        retVal.reserves.push (x) 
      }
      extraSundays -= retVal.reserves.length
      if (extraSundays > 0) {
        let x = 175 - (extraSundays * 7)
        for (var i = x; i < 169; i += 7) {
          retVal.reserves.push(i)
        }
      }
    }

    const noparamias = []
    const getparamias = []
    const paMonths = [2,2,3,3,4,4,4,4]
    const paDays = [24,27,9,31,7,23,25,30]
    paMonths.forEach(function(value, index){
      let xp = gregorianToJd(year, value, paDays[index]) - retVal.paschaJd
      let xd = getDayOfWeek(xp)
      if (xp > -44 && xp < -7 && xd > 1) {
        noparamias.push (xp)
        getparamias.push (xp -1)
      }
    })

    retVal.floats = floats
    retVal.noParamias = noparamias
    retVal.getParamias = getparamias

    return retVal
    }

    if(caching){
      calculateYear.cache = calculateYear.cache || {}
      if(!calculateYear.cache[year]){
        console.log(`doing calculation for ${year}`)
        calculateYear.cache[year] = calculate(year)
      }
      return calculateYear.cache[year]
    }else {
      return calculate(year)
    }
   
    

  }

  const getSynaxisOfUnmercenaries = (pascha, year) => {
    let j = gregorianToJd(year, 11, 1) - pascha
    let weekDay = getDayOfWeek(j)
    return (j + 7 - weekDay)
  }
  const getDemetriusSaturday = (pascha, year) => {
    let j = gregorianToJd(year, 10, 26) - pascha
    let weekDay = getDayOfWeek(j)
    return (j - weekDay - 1)
  }

  const getFathersSeven = (pascha, year) => {
    let j = gregorianToJd(year, 10, 11) - pascha
    let weekday = getDayOfWeek(j)
    if (weekday > 0) {
      j += 7 - weekday
    }
    return j
  }
  const getFathersSix = (pascha, year) => {
    let j = gregorianToJd(year, 7, 16) - pascha
    let weekday = getDayOfWeek(j)
    if (weekday < 4) {
      j -= weekday
    } else {
      j += 7 - weekday
    }
    return j
  }

  const prevSaturday = (pDay, weekDay) => {
    return pDay - weekDay - 1;
  }

  const prevSunday = (pDay, weekDay) => {
    return pDay - 7 + ((7 - weekDay) % 7);
  }

  const upcomingSaturday = (pDay, weekDay) => {
    return pDay + 7 - ((weekDay + 1) % 7);
  }

  const upcomingSunday = (pDay, weekDay) => {
    return pDay + 7 - weekDay;
  }

  const getException = (month, day, year) => {
    var returnVal = {
      newDayException: 0,
      newMonthException: 0,
      exceptionNote: '',
      fastingException: 0
    }
    const exceptItem = exceptionSrvc().getByDate(month, day, year)
    if(exceptItem){
      returnVal.newDayException = exceptItem.xcNewDay
      returnVal.newMonthException = exceptItem.xcNewMonth
      returnVal.exceptionNote = exceptItem.xcNote
      if(exceptItem.xcNewDay < 99){
        const day = daysSrvc.getByMonthDay(exceptItem.xcNewMonth, exceptItem.xcNewDay)
        returnVal.fastingException = day.daFexc
      }
    }

    return returnVal

  }

  const getWeekCountDisplay = (o, week) => {
    let retVal = ''
    if ( o === 1){
      if ( week > 10 && week < 20) {
        retVal = week.toString() + 'th'
      }else{
        retVal = week.toString() + 'st'
      }
    }else if ( o === 2){
      if ( week > 10 && week < 20) {
        retVal = week.toString() + 'th'
      }else{
        retVal = week.toString() + 'nd'
      }
    } else if ( 0 === 3){
      if ( week > 10 && week < 20) {
        retVal = week.toString() + 'th'
      }else{
        retVal = week.toString() + 'rd'
      }
    } else {
      retVal = week.toString() + 'th'
    }
    return retVal
  }

  const calculateFasting = (fast, level, feastLevel , dayOfWeek, pDay, pDayPeterPaul, pDayNativity, pDayTheophany) => {
    let retVal = {
      fast,
      level,
      description: ''
    }
    if (level === 11) {
      retVal.level = level
      return retVal
    }
    // Lent, remove fish for minor feasts
    if (fast === 2) {
      if (level === 2){
        retVal.level = 1
        return retVal
      }
    }
    // Dormition
    if (fast === 4) {
      if ( (dayOfWeek === 0 || dayOfWeek === 6) && level === 0) {
        retVal.level = 1
        return retVal
      }
    }
    // are we in Apostles fast
    if (pDay > 56 && pDay < pDayPeterPaul) {
      fast = 3
      retVal.fast = 3
      if (pDay === 57 && pDay < pDayPeterPaul) {
        retVal.description = "Beginning of Apostles&rsquo; Fast"
      }
    }

    if ( fast === 3 || fast === 5){
      if (dayOfWeek === 2 || dayOfWeek === 4){
        if (!level) { level =  1}
      } else if (dayOfWeek === 3 || dayOfWeek === 5){
        if (feastLevel < 4 && level > 1) { level = 1}
      } else if (dayOfWeek === 0 || dayOfWeek === 6) {
        level = 2
      }
      if (pDay > (pDayNativity - 6) && pDay < (pDayNativity - 1) && level > 1){
        level = 1
      }
    }

    if ((pDay === (pDayNativity - 1) || pDay === (pDayTheophany - 1)) && (dayOfWeek === 0 || dayOfWeek === 6)) {
      level = 1
    }

    retVal.level = level

    return retVal
  }

  const convertToOldCalendar = (month, day, year) => {
    var gregJd = gregorianToJd(year, month, day)
    // an array that looks like [2019, 8, 28]
    var dateArr = jdToJulian(gregJd)
    return {
      year: dateArr[0],
      month: dateArr[1],
      day: dateArr[2]
    }
  }
  
  /**
   * 
   * values returned by calculateDay() function
   * month => month (1-12)
   * day => day
   * year => calendar year
   * pyear => paschal year
   * jd => Julian Day Count
   * pday => pday
   * dow => weekday (0-6)
   * nday => pday in relation to next Pascha
   * vday => pday in relation to previous Pascha
   * gday => pday for gospel
   * eday => pday for epistle
   * jump => days we actually jumped because of Lucan jump
   * of_luke => gospel number of Luke
   * fday => floating feast index 
   * twentynine => boolean if we should get 2/29 from menaion on 2/28
   * no_memorial => boolean if memorial Saturday is cancelled
   * menaion_month => month to lookup in readings table
   * menaion_day => mday to lookup in readings table
   * menaion_note => note for xceptions
   * pname => name of pday (e.g., "29th Sunday after Pentecost")
   * fname => name of feast
   * snote => service note
   * saint => name of saint
   * service => service code
   * feast_level => feast level (see days table)
   * saint_level => saint level (see days table)
   * fast => fasting level (see days table)
   * fast_level => fasting exceptions(see days table)
   * pbase => base pday used for calculating tone and matins gospel
   * tone => Sunday tone
   * no_matins_gospel => boolean to ignore matins gospel (minor feasts on Sundays)
   * matins_gospel => Sunday matins gospel
   * no_paremias => boolean to ignore paremias (Lent)
   * get_paremias => boolean to get paremias from next day (Lent)
   * katavasia => katavasia for canon
   * 
   * calculateDay
   * 
   * @param {number} month 
   * @param {number} day 
   * @param {number} year 
   * 
   */
  const calculateDay = (month = 0, day = 0, year = 0) => {
    if (!month || !day || !year){
      //defaulting to todays date
      var today = new Date()
      month = today.getMonth() + 1 
      day = today.getDate()
      year = today.getFullYear()
    }
    const calculate = (month, day, year) => {

      let pYear = year
      let julianDateCount = gregorianToJd(year, month, day)
      console.log(`julian date count is ${julianDateCount} for month: ${month} day: ${day} year: ${year}`)
      let pDay = julianDateCount - calculatePascha(year)
      if (pDay < -77) {
        --pYear
        pDay = julianDateCount - calculatePascha((year - 1))
      }
      let dayOfWeek = getDayOfWeek(pDay)
      let yearData = calculateYear(pYear)
      // nday is pDay relative to next Pascha, used for theophany stepback
      // vday is pday relative to last Pascha, for tones and matins gospels
      // before palm sunday
      let nDay = julianDateCount - yearData.nextPaschaJd
      let vDay = julianDateCount - yearData.previousPaschaJd
  
      let jump = setJump(pDay, yearData, DO_JUMP);
      let ofLuke = setOfLuke(pDay, jump, yearData, dayOfWeek);
  
      let gDay = 0
      let eDay = 0
      if (yearData.noDaily.includes(pDay)) {
        gDay = 499
        eDay = 499
      } else {
        let limit = 272
        if (pDay === 252) {
          eDay = yearData.foreFathers // forefathers swap
        } else if (pDay > limit) {
          eDay = nDay // theophany stepback
        } else {
          eDay = pDay
        }
        if (yearData.theophanyWeekDay < 2) {
          limit = 279
        }
        if (pDay === (245 - yearData.lucanJump)) {
          gDay = yearData.foreFathers + yearData.lucanJump
        } else if (pDay > yearData.sundayAfterTheophany && dayOfWeek === 0 && yearData.extraSundays > 1) {
          let i = (pDay - yearData.sundayAfterTheophany) / 7
          gDay = yearData.reserves[ i + 1 ]
        } else if ((pDay + jump) > limit){
          gDay = nDay // theophany stepback
        } else {
          gDay = pDay + jump
        }
      }
  
        let fDay = getFloatingFeastDay(yearData, pDay, month, day, dayOfWeek);
  
        let noParamias = isNoParamias(yearData.noParamias, pDay);
        let getParamias = isGetParamias(yearData.getParamias, pDay);
        
        let twentyNine = leapGregorian(year)
  
        let noMemorial = isNoMemorialDay(pDay, month, day);
  
        let exceptions = getException(month, day, year)
        let menaionMonth = exceptions.newMonthException
        let menaionDay = exceptions.newDayException
        let menaionNote = exceptions.exceptionNote
        let menaionFexc = exceptions.fastingException
  
        const alt = (fDay && fDay !== 499)? fDay : null
        const dayRecords = daysSrvc().getDaysForDayCalculation (pDay, alt, month, day)
        log(dayRecords)
        let dayRecordReturn = {
          recordNotes: [],
          recordChurchDayNames: [],
          recordFeastNames: [],
          recordSaints: [],
          pName: '',
          fName: '',
          sNote: '',
          saint: '',
          kata: '',
          service: 0,
          feastLevel: -2,
          saintLevel: 0,
          fast: 0,
          fastLevel: 0
        }
       
        dayRecords.forEach((record) => {
          if (record.daPsub) { record.daPname += ": " + record.daPsub }
          if (record.daPname) { dayRecordReturn.recordChurchDayNames.push(record.daPname) }
          if (record.daFname && (!noMemorial || record.daFname !== 'Memorial Saturday')) {
            dayRecordReturn.recordFeastNames.push(record.daFname)
          }
          if (record.daFlevel > dayRecordReturn.feastLevel) {
            dayRecordReturn.feastLevel = record.daFlevel
          }
          if (record.daSlevel > dayRecordReturn.saintLevel) {
            dayRecordReturn.saintLevel = record.daSlevel
          }
          if (record.daService > dayRecordReturn.service) { 
            dayRecordReturn.service = record.daService 
          }
          if (yearData.annunciation !== pDay) {
            dayRecordReturn.recordNotes.push(record.daSnote)
          }
          if (record.daFast > dayRecordReturn.fast) {
            dayRecordReturn.fast = record.daFast
          }
          if (record.daFexc > dayRecordReturn.fastLevel) {
            dayRecordReturn.fastLevel = record.daFexc
          }
          if (dayRecordReturn.kata.length === 0) {
            dayRecordReturn.kata = record.daKatavasia
          }
          dayRecordReturn.recordSaints.push(record.daSaint)
        })
  
        if (menaionFexc > dayRecordReturn.fastLevel) {
          dayRecordReturn.fastLevel = menaionFexc
        }
  
        if (noParamias && (dayOfWeek === 2 || dayOfWeek === 4)) {
          dayRecordReturn.recordNotes.push('Presanctified Liturgy')
        }
  
        if (dayRecordReturn.recordChurchDayNames.length > 1) {
          dayRecordReturn.pName = dayRecordReturn.recordChurchDayNames.join('; ')
        } else if (dayRecordReturn.recordChurchDayNames.length === 1) {
          dayRecordReturn.pName = dayRecordReturn.recordChurchDayNames[0]
        }
  
        if (dayRecordReturn.recordNotes.length > 1) {
          dayRecordReturn.sNote = dayRecordReturn.recordNotes.join('; ')
        } else if (dayRecordReturn.recordNotes.length === 1) {
          dayRecordReturn.sNote = dayRecordReturn.recordNotes[0]
        }
  
        if (dayRecordReturn.recordSaints.length > 1) {
          dayRecordReturn.saint = dayRecordReturn.recordSaints.join('; ')
        } else if (dayRecordReturn.recordSaints.length === 1) {
          dayRecordReturn.saint = dayRecordReturn.recordSaints[0]
        }
  
        if (dayRecordReturn.recordFeastNames.length > 1) {
          dayRecordReturn.fName = dayRecordReturn.recordFeastNames.join('; ')
        } else if (dayRecordReturn.recordFeastNames.length === 1) {
          dayRecordReturn.fName = dayRecordReturn.recordFeastNames[0]
        }
  
        if (dayOfWeek < 1 || dayRecordReturn.feastLevel > 2 || dayRecordReturn.saintLevel > 2) {
          if (dayRecordReturn.kata.length === 0) {
            dayRecordReturn.kata = '&ldquo;I will open my mouth...&rdquo;'
          }
        }
  
        assert.equal(dayRecordReturn !== null, true, "this should be equal")
        assert.equal( dayRecordReturn.pName !== null, true, "this should be equal")
  
        //calculat sunday tone
        let pBase = 0
       
        let tone = 0
        let mBase = 0
        
  
        if (pDay < 0) {
          pBase = vDay
        } else {
          pBase = pDay
        }
        if (pDay > -9 && pDay < 7) {
          tone = 0
        } else {
          let x = pBase % 56
          if (x === 0) {
            x = 56
          }
          tone = Math.floor (x / 7) 
        }
        //calculate sunday matins gospel
        let noMatinsGospel = false
        let matinsGospel = 0
        if (dayOfWeek === 0) {
          //its sunday
          if(pDay > -8 && pDay < 50) {
            noMatinsGospel = true
          } else if (dayRecordReturn.feastLevel < 7) {
            noMatinsGospel = true
            mBase = pBase - 49
            let x = mBase % 77
            if (x === 0){
              x = 77
            }
            matinsGospel = Math.floor (x / 7)
          }
        }
        //debugger
        //calculate pentecost out of range
       //calculate pentecost out of range
        if (pDay < -48){
          let pent = calculatePentecost(pBase)
          if (dayRecordReturn.pName) {
            dayRecordReturn.pName = pent + ': ' + dayRecordReturn.pName
          } else {
            dayRecordReturn.pName = pent
          }
        }
        assert.equal(dayRecordReturn !== undefined, true, "this should be equal")
        assert.equal( dayRecordReturn.pName !== undefined, true, "this should be equal")
  
        //get fasting
        const fastingList = calculateFasting(dayRecordReturn.fast, dayRecordReturn.fastLevel, dayRecordReturn.feastLevel, dayOfWeek, pDay, yearData.pDayPeterPaul, yearData.pDayNativity, yearData.pDayTheophany )
        if (fastingList.description) {
          if (dayRecordReturn.sNote) {
            dayRecordReturn.sNote = fastingList.description + ": " + dayRecordReturn.sNote
          } else {
            dayRecordReturn.sNote = fastingList.description
          }
        }
  
        let retVal = {
          month: month,
          day: day,
          year: year,
          pYear: pYear,
          jd: julianDateCount,
          pDay: pDay,
          dayOfWeek: dayOfWeek,
          nDay: nDay,
          vDay: vDay,
          gDay: gDay,
          eDay: eDay,
          jump: jump,
          ofLuke: ofLuke,
          fDay: fDay,
          twentyNine: twentyNine,
          noMemorial: noMemorial,
          menaionMonth: menaionMonth,
          menaionDay: menaionDay,
          menaionNote: menaionNote,
          pName: dayRecordReturn.pName,
          fName: dayRecordReturn.fName,
          sNote: dayRecordReturn.sNote,
          saint: dayRecordReturn.saint,
          service: dayRecordReturn.service,
          feastLevel: dayRecordReturn.feastLevel,
          saintLevel: dayRecordReturn.saintLevel,
          fast: dayRecordReturn.fast,
          fastLevel: dayRecordReturn.fastLevel,
          fastLevelDescription: FastLevels[dayRecordReturn.fastLevel],
          pBase: pBase,
          tone: tone,
          noMatinsGospel: noMatinsGospel,
          matinsGospel: matinsGospel,
          noParamias: noParamias,
          getParamias: getParamias,
          katavasia: dayRecordReturn.kata
        }
  
        return retVal

    }


    const key = String(month) + String(day) + String(year)
    if (caching) {
      calculateDay.cache = calculateDay.cache || {}
      if(!calculateDay.cache[key]){
        console.log('doing calc for ', key)
        calculateDay.cache[key] = calculate(month, day, year)
      }
      return calculateDay.cache[key]

    }else{
      return calculate(month, day, year)
    }
   

  }


  const leapYear = (year) => {
    return year % 100 === 0 ? year % 400 === 0 : year % 4 === 0;
  }
  const calculatePentecost = (pDay) => {
    let retVal = ''
    let week = Math.floor (pDay / 7)
    let day = pDay % 7
    if (day === 0) {
      week--
    }
    
    let o = week % 10
    var weekCount = getWeekCountDisplay (o, week)

    if (day === 0) {
      retVal = `${weekCount} Sunday after Pentecost`
    } else if (day === 1) {
      retVal = `Monday of the ${weekCount} week after Pentecost`
    } else if (day === 2) {
      retVal = `Tuesday of the ${weekCount} week after Pentecost`
    } else if (day === 3) {
      retVal = `Wednesday of the ${weekCount} week after Pentecost`
    } else if (day === 4) {
      retVal = `Thursday of the ${weekCount} week after Pentecost`
    } else if (day === 5) {
      retVal = `Friday of the ${weekCount} week after Pentecost`
    } else if (day === 6) {
      retVal = `Saturday of the ${weekCount} week after Pentecost`
    }
    return retVal
  }

  const leapGregorian = (year) => {
    return ((year % 4) == 0) &&
      (!(((year % 100) == 0) && ((year % 400) != 0)));
  }

  const getJulianDateCount = (year, month, day) => {
    return gregorianToJd(year, month, day)
  }

  const julianToJd = (year, month, day) => {
    // Adjust negative common era years to the zero-based notation we use.
    if (year < 1) { year++; }

    // Algorithm as given in *Meeus, **Astronomical Algorithms**, Chapter 7, page 61*
    if (month <= 2) {
      year--;
      month += 12;
    }

    return ((Math.floor((365.25 * (year + 4716))) +
      Math.floor((30.6001 * (month + 1))) +
      day) - 1524.5);
  }

  const gregorianToJd = (year, month, day) => {
    const retVal = (GREGORIAN.EPOCH - 1) +
      (365 * (year - 1)) +
      Math.floor((year - 1) / 4) +
      (-Math.floor((year - 1) / 100)) +
      Math.floor((year - 1) / 400) +
      Math.floor((((367 * month) - 362) / 12) +
        ((month <= 2) ? 0 :
          (leapGregorian(year) ? -1 : -2)
        ) +
        day);
    return Math.round(retVal)
  }

  return {
    calculateYear,
    calculateDay,
    getDayOfWeek,
    prevSunday,
    prevSaturday,
    upcomingSaturday,
    upcomingSunday,
    getException,
    calculatePascha,
    calculatePentecost,
    calculateFasting,
    jdToJulian,
    gregorianToJd,
    convertToOldCalendar,
    caching
  }
}

function isNoMemorialDay(pDay, month, day) {
  let retVal = false
  if ((pDay === -36 || pDay === -29 || pDay === -22) &&
    month === 3 &&
    (day === 9 || day === 24 || day === 25 || day === 26)) {
    retVal = true;
  }
  return retVal;
}

function isGetParamias(getParamiasList, pDay) {
  return (getParamiasList.indexOf(pDay) > -1) 
}

function isNoParamias(noParamiasList, pDay) {
  return (noParamiasList.indexOf(pDay) > -1);
}

function getFloatingFeastDay(yearData, pDay, month, day, dayOfWeek) {
  let retVal = 0
  if (yearData.floats.indexOf(pDay) > -1) {
    retVal = pDay;
  }
  if (month === 1 && day > 24 && dayOfWeek === 0) {
    retVal = 1031;
  }
  return retVal;
}

function setOfLuke(pDay, jump, yearData, dayOfWeek) {
  let retVal = 0
  let ld = pDay + jump;
  if (ld > 168 && ld < 274 && pDay < yearData.nativity && dayOfWeek === 0) {
    retVal = Math.floor((ld - 168) / 7);
  }
  return retVal;
}

function setJump(pDay, yearData, DO_JUMP) {
  let retVal = 0
  if (pDay > yearData.sundayAfterElevation && DO_JUMP > 0) {
    retVal = yearData.lucanJump;
  }
  return retVal;
}

