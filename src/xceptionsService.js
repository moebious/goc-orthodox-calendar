//var exceptions = require('./data/xceptions.js')
import exceptions from './data/xceptions'

export default () =>{
  const getByDate = (month, day, year) => {
    
    var item = exceptions.find((value) => {
      return (value.xcYear === year && value.xcMonth === month && value.xcDay === day)
    })
    
    return item
  }

  return {
    getByDate: getByDate
  }

}

//module.exports = exceptionSrvc()