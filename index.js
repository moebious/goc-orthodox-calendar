import core from './src/core'
import readingsService from './src/readingsService'
import daysService from './src/daysService'

let coreLib = core()

export default () => {
  return {
    coreLib,
    readingsService,
    daysService
  }
}
